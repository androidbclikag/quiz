package com.example.quiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var firstName: MutableList<String>
    private lateinit var lastnName: MutableList<String>
    private lateinit var ageList: MutableList<Int>
    private lateinit var emailList: MutableList<String>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    //ლოგქათში ვერ ვნახულობ რა ხდება რაღაც პრობლემის გამო ამიტომ რატომ იქრაშება ვერ ვხვდები :/


    public fun init() {
        addUser.setOnClickListener {
            if (firstNameEditText.text.isNotEmpty() && lastNameEditText.text.isNotEmpty() && ageEditText.text.isNotEmpty() && emailEditText.text.isNotEmpty()) {
                checkUser()
            }
            else
                Toast.makeText(this, "please fill all fields", Toast.LENGTH_SHORT).show()
        }


    }

    private fun checkUser() {
        val newUserEmail = emailEditText.text.toString()
        for (item in emailList) {
            if ( item == newUserEmail) {
                Toast.makeText(this, "$newUserEmail already exists", Toast.LENGTH_SHORT).show()

            } else {
                addName()
                addLastName()
                addAge()
                addEmail()
            }


        }
    }


    private fun addName() {
        val name: String = firstNameEditText.text.toString()
        firstName.add(name)
    }

    private fun addLastName() {
        val surname = lastNameEditText.text.toString()
        lastnName.add(surname)
    }

    private fun addAge() {
        val age = ageEditText.text.toString().toInt()
        ageList.add(age)
    }

    private fun addEmail() {
        val email = emailEditText.text.toString()
        emailList.add(email)
    }



}
